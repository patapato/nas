*****************************************************************************
**Changes in NAS 0.8.13(N1) relative to NXT 0.8.13 commit 7313903 (titled
`prevent duplicate hash transactions in the unconfirmed transactions`, 
2 commits before the final labeled one).**

All the files (except this one) are the ones in the original 
distribution of the creator of NAS, Nas_Coin@bitcointalk.com 
(see <https://bitcointalk.org/index.php?topic=523187.0> 
and search: _0.8.13(N1) Full source_).
*****************************************************************************

This file explains the differences between NAS coin and NXT coin. 
It is in Markdown format, using `*` to list files, `-` for other lists. 
The important differences are the ones which implies a change in the 
blockchain database structure, or a change in the first block. 

Mayor changes, which defines the coin:
-------------------------------------

### 1. Peer server port 7871:
* _src/java/nxt/peer/Peers.java_ (`DEFAULT_PEER_PORT = 7871`)
* _conf/nxt-default.properties_ (`nas.peerServerPort=7871`)

### 2. Genesis block ID and signature`, and initial distribution of coins:
* _src/java/Genesis.java_

### 3. Timestamp origin set to 2014 march 25 12:00.000:
* _src/java/nxt/Constants.java_ (`EPOCH_BEGINNING`):
* _html/nrs/myTransaction.html_ (**bug in month, and hardcoded!**, `Date.UTC(2014, 2, 25, 12, 0, 0, 0)`)
* _html/nrs/orphanedBlocks.html_ (**same bug!**)
* _html/nrs/recentBlocks.html_ (**same bug!**)
* _html/nrs/unconfirmedTransactions.html_ (**same bug!**)

### 4. Total amount of coins is 10 billion NAS: 
* _src/java/nxt/Constants.java_ (`MAX_BALANCE=10000000000L`)
* _src/java/nxt/BlockchainProcessorImpl.java_: `10000000000L` **hardcoded!** in `addGenesisBlock()`

#### The next changes are a consecuence of it:
- `BIGINT` instead of `INT` in DB fields `block.total_amount` and `transaction.amount`:
  * _src/java/DbVersion.java_
- `Long` instead of `int` in amount variables, postfix `L` in numeric values 
  (in all uses of `amount`, `getAmount()`, `totalAmount`, `getTotalAmount()`):
    * _src/java/nxt/Block.java_
    * _src/java/nxt/BlockDb.java_
    * _src/java/nxt/BlockImpl.java_ (buffer new size **hardcoded!** in `getBytes()`: 128 + 4 = 132)
    * _src/java/nxt/BlockchainProcessorImpl.java_
    * _src/java/nxt/DbVersion.java_
    * _src/java/nxt/Transaction.java_
    * _src/java/nxt/TransactionDb.java_
    * _src/java/nxt/TransactionImpl.java_ (also, `TRANSACTION_BYTES_LENGTH`; and two `for` limits **harcoded!** in `getHash()` and `verify()`)
    * _src/java/nxt/TransactionProcessor.java_
    * _src/java/nxt/TransactionProcessorImpl.java_
    * _src/java/nxt/TransactionType.java_ (also, **hardcoded 30!** at line 561, it should be the original)
    * _src/java/nxt/http/CreateTransaction.java_
    * _src/java/nxt/http/SendMessage.java_
    * _src/java/nxt/http/SendMoney.java_

Minor changes, not affecting coin definition: 
---------------------------------------------

### Files removed: 
* _.gitignore_
* _MIT-license.txt_
* _changelogs/*_

### Files added: 
* _readme.txt_
* most of _html/tools/\*/\*_ (for the then new experimental UI of _wesleyh_)

### Files modified: 
* _run.bat_: `NXT NRS`-> `NAS NRS`
* _src/java/nxt/BlockchainProcessorImpl.java_
    - `CHECKSUM_TRANSPARENT_FORGING = null` (why?)
* _src/java/nxt/Constants.java_, reset of features block heights:
    - `ALIAS_SYSTEM_BLOCK = 0;`
    - `TRANSPARENT_FORGING_BLOCK = 1;`
    - `ARBITRARY_MESSAGES_BLOCK = 0;`
* _src/java/DbVersion.java_: not `import java.util.List`
* _src/java/nxt/Nxt.java_:
    - Rename of properties files from `nxt*.properties` to `nas*.properties`
    - `VERSION = "0.8.13.N1"`
    - Final log messages
* _src/java/nxt/peer/Peers.java_:
    - commented out `for` to get random known peers in case of no well known 
      peers defined;  substituted by `addresses.add("nascoin.no-ip.biz")`
* _conf/nxt-default.properties_:
    - `nas.wellKnownPeers=192.126.123.112;162.211.181.47`
    - `nas.testnetPeers=bug.airdns.org`
    - `nas.log=nas.log`
    - `nas.debug=true`
    - Rename of properties from `nxt.*` to `nas.*` (all uses of 
      `nxt.getBooleanProperty`, `nxt.getIntProperty`, 
      `nxt.getStringProperty`, `nxt.getStringListProperty`):
        * _src/java/nxt/Db.java_
        * _src/java/nxt/DebugTrace.java_
        * _src/java/nxt/Nxt.java_
        * _src/java/nxt/VerifyTrace.java_
        * _src/java/nxt/http/API.java_
        * _src/java/nxt/http/APIServlet.java_
        * _src/java/nxt/peer/Peers.java:_
        * _src/java/nxt/user/UserServlet.java_
        * _src/java/nxt/user/Users.java_
        * _src/java/nxt/util/Logger.java_

### Only spacing or position changes, or extra imports:
* _src/java/nxt/http/BroadcastTransaction.java_
* _src/java/nxt/http/GetAllOpenOrders.java_ (extra `import nxt.util.Convert`)
* _src/java/nxt/http/GetUnconfirmedTransactionsIds.java_
* _src/java/nxt/http/JSONResponses.java_
* _src/java/nxt/http/TransferAsset.java_
