# Do not modify this file. Instead, create a nas.properties file with only the properties
# you need to change, and set them there. The values in nas.properties override those in
# nas-default.properties.



#### PEER NETWORKING ####

# Announce my IP address/hostname to peers and allow them to share it with other peers.
# If disabled, peer networking servlet will not be started at all.
nas.shareMyAddress=true

# Port for incoming peer to peer networking requests, if enabled.
nas.peerServerPort=7871

# Host interface on which to listen for peer networking requests, default all.
# Use 0.0.0.0 to listen on all IPv4 interfaces or :: to listen on all IPv4 and IPv6 interfaces
nas.peerServerHost=0.0.0.0

# My externally visible IP address or host name, to be announced to peers.
# It can optionally include a port number, which will also be announced to peers,
# and may be different from nas.peerServerPort (useful if you do port forwarding behind a router).
nas.myAddress=

# My platform, to be announced to peers.
nas.myPlatform=PC

# My hallmark, if available.
nas.myHallmark=

# A list of well known peer addresses / host names, separated by '; '.
nas.wellKnownPeers=

# Known bad peers to be blacklisted
nas.knownBlacklistedPeers=

# Peers used for testnet only.
nas.testnetPeers=

# Maintain active connections with at least that many peers.
nas.maxNumberOfConnectedPublicPeers=20

# Peer networking connect timeout for outgoing connections.
nas.connectTimeout=10000

# Peer networking read timeout for outgoing connections.
nas.readTimeout=20000

# Peer networking server idle timeout, milliseconds.
nas.peerServerIdleTimeout=30000

# Use the peer hallmark to only connect with peers above the defined push/pull hallmark thresholds.
# Disabling hallmark protection also disables weighting of peers by hallmark weight, so connecting
# to any of your peers becomes equally likely.
nas.enableHallmarkProtection=true

# Hallmark threshold to use when sending data to peers.
nas.pushThreshold=0

# Hallmark threshold to use when requesting data from peers.
nas.pullThreshold=0

# Blacklist peers for 600000 milliseconds (i.e. 10 minutes by default).
nas.blacklistingPeriod=600000

# Consider a new transaction or block sent after 10 peers have received it.
nas.sendToPeersLimit=10

# Enable the Jetty Denial of Service Filter for the peer networking server.
nas.enablePeerServerDoSFilter=true

# Use testnet, leave set to false unless you are really testing.
# Never unlock your real accounts on testnet! Use separate accounts for testing only.
# When using testnet, all custom port settings will be ignored,
# and hardcoded ports of 6871 (peer networking), 6872 (UI) and 6873 (API) will be used.
nas.isTestnet=false

# Save known peers in the database
nas.savePeers=true

# Set to false to disable use of the peers database. This will not delete saved peers.
nas.usePeersDb=true



#### API SERVER ####

# Accept http/json API requests.
nas.enableAPIServer=true

# Hosts from which to allow http/json API requests, if enabled. Set to * to allow all.
nas.allowedBotHosts=127.0.0.1; localhost; [0:0:0:0:0:0:0:1];

# Port for http/json API requests.
nas.apiServerPort=7873

# Host interface on which to listen for http/json API request, default localhost only.
# Set to 0.0.0.0 to allow the API server to accept requests from all network interfaces.
nas.apiServerHost=127.0.0.1

# Idle timeout for http/json API request connections, milliseconds.
nas.apiServerIdleTimeout=30000

# Directory with html and javascript files for the new client UI, and admin tools utilizing
# the http/json API.
nas.apiResourceBase=html/ui

# Java API documentation directory, optional.
nas.javadocResourceBase=html/doc

# Enable Cross Origin Filter for the API server.
nas.apiServerCORS=false

# Enable SSL for the API server (also need to set nas.keyStorePath and nas.keyStorePassword).
nas.apiSSL=false

# Enforce requests that require POST to only be accepted when submitted as POST.
nas.apiServerEnforcePOST=true



#### OLD NRS USER INTERFACE ####

# Enable the deprecated NRS user interface.
nas.enableUIServer=true

# Hosts from which to allow NRS user interface requests, if enabled. Set to * to allow all.
nas.allowedUserHosts=127.0.0.1; localhost; [0:0:0:0:0:0:0:1];

# Port for NRS user interface server.
nas.uiServerPort=7872

# Host interface for NRS user interface server, default localhost only.
# Set to 0.0.0.0 to allow the UI to be accessed on all network interfaces.
nas.uiServerHost=127.0.0.1

# Idle timeout for NRS user interface server, milliseconds.
nas.uiServerIdleTimeout=30000

# Directory with html and javascript files for the NRS client user interface.
nas.uiResourceBase=html/nrs

# Enable Cross Origin Filter for NRS user interface server.
nas.uiServerCORS=false

# Enable SSL for the NRS user interface (also need to set nas.keyStorePath and nas.keyStorePassword).
nas.uiSSL=false

# Enforce requests that require POST to only be accepted when submitted as POST.
nas.uiServerEnforcePOST=true



#### DEBUGGING ####

# Log file, will be overwritten at every restart.
#nas.log=nas.log
# The log file is now specified in logging.properties.

# Enable debug log output.
#nas.debug=false
# Debug logging is now specified in logging properties.

# Include caller traceback in log messages.
nas.enableLogTraceback=false

# Enable logging of exception stack traces.
nas.enableStackTraces=true

# Enable logging of Jetty messages.
#nas.debugJetty=false
# Jetty logging is now specified in logging properties.

# Used for debugging peer to peer communications.
nas.communicationLoggingMask=0

# Track balances of the following accounts and related events for debugging purposes.
nas.debugTraceAccounts=

# File name for logging tracked account balances.
nas.debugTraceLog=nas-trace.csv

# Separator character for trace log.
nas.debugTraceSeparator=\t

# Quote character for trace log.
nas.debugTraceQuote="

# Log changes to unconfirmed balances.
nas.debugLogUnconfirmed=false


#### DATABASE ####

# Database connection JDBC url, see the H2 documentation for possible customizations.
# Append ;AUTO_SERVER=TRUE to enable automatic mixed mode access.
# The nxt_db folder is expected to be in the current working directory, will be created if missing.
nas.dbUrl=jdbc:h2:nxt_db/nxt;DB_CLOSE_ON_EXIT=FALSE;MVCC=TRUE

# Database connection JDBC url to use with the test network, if isTestnet=true
nas.testDbUrl=jdbc:h2:nxt_test_db/nxt;DB_CLOSE_ON_EXIT=FALSE;MVCC=TRUE

# Database connection timeout in seconds.
nas.dbLoginTimeout=70

# Database default lock timeout in seconds.
nas.dbDefaultLockTimeout=60

# Maximum simultaneous database connections.
nas.maxDbConnections=30

# The memory allocated to database cache, in kB.
# If set to 0, defaults to 50 % of the memory available to the JVM.
nas.dbCacheKB=0



#### JETTY ####

# Settings for the Jetty Denial Of Service Filter, used for the peer networking server only.
nas.peerServerDoSFilter.maxRequestsPerSec=30
nas.peerServerDoSFilter.delayMs=1000
nas.peerServerDoSFilter.maxRequestMs=300000

# keystore file and password, required if uiSSL or apiSSL are enabled.
nas.keyStorePath=keystore
nas.keyStorePassword=password



#### Developers only ####

# Force re-validation of blocks and transaction at start.
nas.forceValidate=false

# Print a list of peers having this version on exit.
nas.dumpPeersVersion=

